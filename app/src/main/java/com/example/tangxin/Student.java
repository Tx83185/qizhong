package com.example.tangxin;

import java.io.Serializable;

public class Student implements Serializable {
    private String id;
    private String name;
    private String gender;
    private String className;

    public Student(String id, String name, String gender, String className) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.className = className;
    }


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getClassName() {
        return className;
    }


    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
