<<<<<<< HEAD
package com.example.tangxin

import android.os.Bundle
=======
package com.example.tangxin;

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Button
>>>>>>> ed2b7ed (test)
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
<<<<<<< HEAD

class MainActivity : AppCompatActivity() {
=======
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var studentAdapter: StudentAdapter
    private var studentList = mutableListOf<Student>()
    private val ADD_STUDENT_REQUEST = 1
    private lateinit var sharedPreferences: SharedPreferences

>>>>>>> ed2b7ed (test)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
<<<<<<< HEAD
=======

        sharedPreferences = getSharedPreferences("student_prefs", MODE_PRIVATE)

>>>>>>> ed2b7ed (test)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
<<<<<<< HEAD
    }
}
=======

        recyclerView = findViewById(R.id.recyclerView)
        studentAdapter = StudentAdapter(studentList)
        recyclerView.adapter = studentAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        // 加载学生信息
        loadStudentData()

        val addButton: Button = findViewById(R.id.addButton)
        addButton.setOnClickListener {
            val intent = Intent(this, AddStudentActivity::class.java)
            startActivityForResult(intent, ADD_STUDENT_REQUEST)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ADD_STUDENT_REQUEST && resultCode == RESULT_OK) {
            val newStudent = data?.getSerializableExtra("newStudent") as? Student
            newStudent?.let {
                studentList.add(it)
                studentAdapter.notifyDataSetChanged()
                saveStudentData()
            }
        }
    }

    private fun saveStudentData() {
        val editor = sharedPreferences.edit()
        val studentData = studentList.joinToString(";") { "${it.id},${it.name},${it.gender},${it.className}" }
        editor.putString("student_data", studentData)
        editor.apply()
    }

    private fun loadStudentData() {
        val studentData = sharedPreferences.getString("student_data", "")
        Log.d("MainActivity", "Loaded student data: $studentData")
        studentData?.let {
            val students = it.split(";").mapNotNull { data ->
                val attributes = data.split(",")
                if (attributes.size == 4) {
                    Student(attributes[0], attributes[1], attributes[2], attributes[3])
                } else {
                    null
                }
            }
            studentList.clear()
            studentList.addAll(students)
            studentAdapter.notifyDataSetChanged()
        }
    }
}
>>>>>>> ed2b7ed (test)
