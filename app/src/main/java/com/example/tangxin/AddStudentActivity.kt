package com.example.tangxin;

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class AddStudentActivity : AppCompatActivity() {
    private lateinit var idEditText: EditText
    private lateinit var nameEditText: EditText
    private lateinit var genderEditText: EditText
    private lateinit var classEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_add_student)

        // 绑定 EditText 和 Button
        idEditText = findViewById(R.id.idEditText)
        nameEditText = findViewById(R.id.nameEditText)
        genderEditText = findViewById(R.id.genderEditText)
        classEditText = findViewById(R.id.classEditText)

        val confirmButton: Button = findViewById(R.id.confirmButton)
        confirmButton.setOnClickListener {
            val id = idEditText.text.toString()
            val name = nameEditText.text.toString()
            val gender = genderEditText.text.toString()
            val className = classEditText.text.toString()

            val newStudent = Student(id, name, gender, className)
            val resultIntent = Intent()
            resultIntent.putExtra("newStudent", newStudent)
            setResult(RESULT_OK, resultIntent)
            finish()
        }

        val cancelButton: Button = findViewById(R.id.cancelButton)
        cancelButton.setOnClickListener {
            finish()
        }

        // 处理窗口插入
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }
}
